def print_details(celeb):
    print('First Name: ' + celeb['firstName'])
    print('Last Name: ' + celeb['lastName'])
    print('Gender: ' + celeb['gender'])


def filter_celebrities(celebs, gender):
    filtered_celebs = []

    for celeb in celebs:
        if celeb['gender'] == gender:
            filtered_celebs.append(celeb)

    return filtered_celebs        


# list of celebrities
celebrities = [
    {
        'firstName': 'Jessy',
        'lastName': 'Mendiola',
        'gender': 'Female'
    },
    {
        'firstName': 'Coco',
        'lastName': 'Martin',
        'gender': 'Male'
    }
]

gender = input("Enter a gender: ")

celebrities = filter_celebrities(celebrities, gender)

# call the function
for celeb in celebrities:
    print_details(celeb)
