def celsius_to_kelvin(cel):
    return cel - 273.15


def celsius_to_fahrenheit(cel):
    return (cel * (9 / 5)) + 32


celsius = 100.0

print(celsius, "celsius = ", celsius_to_kelvin(celsius), "kelvin")
print(celsius, "celsius = ", celsius_to_fahrenheit(celsius), "fahrenheit")
