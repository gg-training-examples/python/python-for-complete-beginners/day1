for x in range(1, 10): # loop from 1 to 9
    if x > 5:          # break the loop if x greater than 5
        continue

    if x % 2 == 0:     # print x if x is divisible to 2
        print(x)
