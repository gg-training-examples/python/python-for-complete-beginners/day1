gpa = 89.0

if gpa >= 90.0:
    print("Excellent")
elif gpa >= 80.0 and gpa <= 89.9:
    print("Great")
else:
    print("Better luck next time!")
