numbers = [343, 6564, 324, 65, 32, 6752, 903, 32, 324]
numbers[3] = 405 # replace the value in index 3

print(numbers) # print to see that it's replaced

numbers.sort(reverse=True) # sort in reverse

print(numbers) # print to see the sorted numbers 

print(numbers[1]) # print the numbers in index 1
