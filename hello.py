print("Hello World")

x = 10 + 20 * 30 / 40 - 50
# 10 + 600 / 40 - 50
# 10 + 15 - 50
# 25 - 50
# -25

print(x)

# assigment
y = 20

# x = -25

# equals
print(x == y) # output: False

# not equals
print(x != y) # output: True

# greather than
print(x > y) # output: False

# less than
print(x < y) # output: True
