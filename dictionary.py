actress = {
    'firstName': 'jessy', 
    'lastName': 'mendiola',
    'occupation': 'Celebrity'
}

# get the first character the capitalize
print(actress['firstName'][0].capitalize())

# get the character in index 0 to 5
print(actress['lastName'][0:5])

# more readable
print(actress['firstName'] + ' ' + actress['lastName'])

# not that readable
print(' '.join([actress['firstName'], actress['lastName']]))
